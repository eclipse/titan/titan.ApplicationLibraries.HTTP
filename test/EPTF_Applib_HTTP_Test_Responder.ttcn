///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:     EPTF_Applib_HTTP_Test_Responder.ttcn
//  Rev:      <RnXnn>
//  Prodnr:   CNL 113 618
//  Updated:  2009-01-08 
//  Contact:  http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_Applib_HTTP_Test_Responder
// 
//  Purpose:
//    This module contains the functions to simulate a HTTP
//    responder node for tests
// 
//  Module Parameters:
//    -
// 
//  Module depends on:
//    <HTTPmsg_Types>
//
//    <IPL4asp_Types>
//
//    <IPL4asp_PortType>
//
//    <EPTF_CLL_Common_Definitions>
//
//  Current Owner:    
//    EAKOPER 
// 
//  Last Review Date:
//    2009-01-09
// 
//  Detailed Comments:
//    -
// 
///////////////////////////////////////////////////////////

module EPTF_Applib_HTTP_Test_Responder { 

//=========================================================================
// Import Part
//=========================================================================
import from HTTPmsg_Types all;
import from IPL4asp_Types all;
import from IPL4asp_PortType all;
import from EPTF_CLL_Common_Definitions all;

//=========================================================================
// Module parameters
//=========================================================================
modulepar charstring tsp_EPTF_HTTP_Test_Responder_host := "159.107.193.33";
modulepar integer tsp_EPTF_HTTP_Test_Responder_port := 4000;

//=========================================================================
// Component types
//=========================================================================
type component EPTF_HTTP_Test_Responder_CT {
  port IPL4asp_PT v_IPL4_PCO;
  port EPTF_HTTP_Test_CM_PT v_CM_PCO;

  var EPTF_HTTP_Test_Responder_HostInformationList v_hostDatabase := {};
  var EPTF_IntegerList v_connIds := {};

  var ASP_RecvFrom v_ASP_RecvFrom;
  var ASP_Event v_ASP_Event;

  var HTTPMessage v_HTTP_msg;

  var integer v_intNoWarn;
}

//=========================================================================
// Types
//=========================================================================

type port EPTF_HTTP_Test_CM_PT message {
  inout charstring 
} with {extension "internal"}

type record EPTF_HTTP_Test_Responder_HostInformation {
  charstring hostIPAddress ,
  integer hostPort
}

type record of EPTF_HTTP_Test_Responder_HostInformation EPTF_HTTP_Test_Responder_HostInformationList;

//=========================================================================
// Functions
//=========================================================================
function f_EPTF_HTTP_Test_Responder_init(
  in EPTF_HTTP_Test_Responder_HostInformationList pl_hosts)
runs on EPTF_HTTP_Test_Responder_CT return boolean
{
  var Result vl_result;
  var integer i;

  map(self:v_IPL4_PCO, system:v_IPL4_PCO);

  v_hostDatabase := pl_hosts;  

  for (i := 0; i < sizeof(v_hostDatabase); i := i + 1) 
  {    
    vl_result := f_IPL4_listen(v_IPL4_PCO,       
      v_hostDatabase[i].hostIPAddress, v_hostDatabase[i].hostPort, { tcp := {} });

    if (vl_result.errorCode == omit) {
      log("Listening successful on: ", v_hostDatabase[i], " (", vl_result.connId, ")");      
      v_connIds[i] := vl_result.connId;
    } else {
      log("Listening failed on: ", v_hostDatabase[i], " (", vl_result, ")");
      return false;
    }
  }

  return true;
}

function f_EPTF_HTTP_Test_Responder_uninit()
runs on EPTF_HTTP_Test_Responder_CT
{
  var Result vl_result;
  var integer i;

  for (i := 0; i < sizeof(v_connIds); i := i + 1) 
  {    
    vl_result := f_IPL4_close(v_IPL4_PCO, v_connIds[i], { tcp := {} });
  }

  v_hostDatabase := {};
  v_connIds := {};

  unmap(self:v_IPL4_PCO, system:v_IPL4_PCO);
}

function f_EPTF_HTTP_Test_dummyHTTPResponse(
  in charstring pl_body)
return octetstring
{
  var HTTPMessage vl_outgoingMessage := {
    response := {
      client_id := omit,
      version_major := 1,
      version_minor := 1,
      statuscode := 200,
      statustext := "OK",
      header:= {{"Content-Length", int2str(lengthof(pl_body)) }},
      body:= pl_body
    }
  }

  log("HTTP message: ", vl_outgoingMessage);

  return enc_HTTPMessage(vl_outgoingMessage);
}

function f_EPTF_HTTP_Test_Responder_behavior(
  in integer pl_tcId,
  in float pl_runningTime)
runs on EPTF_HTTP_Test_Responder_CT
{
  log("<< Starting HTTP responder! >>");

  timer t_guard := pl_runningTime;

  var EPTF_HTTP_Test_Responder_HostInformationList vl_hList;
  var ASP_Send vl_send;

  // provide initial data according to TC  
  vl_hList := { { tsp_EPTF_HTTP_Test_Responder_host, tsp_EPTF_HTTP_Test_Responder_port + pl_tcId } };

  // initialize component
  if (not f_EPTF_HTTP_Test_Responder_init(vl_hList)) {
    log("HTTP responder failed during initialization!");

    f_EPTF_HTTP_Test_Responder_uninit();
    setverdict(fail);
    return
  }

  v_CM_PCO.send("Hello, I'm ready! :-)");

  t_guard.start;
  alt {
    [] v_IPL4_PCO.receive(ASP_RecvFrom:?) -> value v_ASP_RecvFrom {
        log("ASP receive from: ", v_ASP_RecvFrom);

        v_intNoWarn := dec_HTTPMessage(v_ASP_RecvFrom.msg, v_HTTP_msg, false);
        log("HTTP payload: ", v_HTTP_msg);

        vl_send := { 
          v_ASP_RecvFrom.connId, {tcp := {}}, 
          f_EPTF_HTTP_Test_dummyHTTPResponse(v_HTTP_msg.request.body)
        };

        v_IPL4_PCO.send(vl_send);        

        repeat;
      }
    [] v_IPL4_PCO.receive(ASP_Event:?) -> value v_ASP_Event {
        log("ASP event: ", v_ASP_Event);
        log(v_IPL4_PCO);
        repeat;
      }
    [] v_IPL4_PCO.receive {
        log("Invalid message received!");
        repeat;
      }
    [] t_guard.timeout {
        log("Timer timed out!");
      }    
  }

  f_EPTF_HTTP_Test_Responder_uninit();

  log("<< Stopping HTTP responder! >>");

  setverdict(pass);
}

}

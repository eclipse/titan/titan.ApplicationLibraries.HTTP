///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:     EPTF_Applib_HTTP_Test_Definitions.ttcn
//  Rev:      <RnXnn>
//  Prodnr:   CNL 113 618
//  Updated:  2009-01-08
//  Contact:  http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
//  Module: EPTF_Applib_HTTP_Test_Definitions
// 
//  Purpose:
//    Definitions of EPTF HTTP Application Library test cases
// 
//  Module Parameters:
//    -
// 
//  Module depends on:
//    <EPTF_CLL_Base_Definitions>
// 
//    <HTTPmsg_Types>
// 
//    <IPL4asp_Types>
// 
//    <EPTF_Applib_HTTP_Responder>
// 
//    <EPTF_Applib_HTTP_Test_Application1_Definitions>
// 
//  Current Owner:
//    EAKOPER
// 
//  Last Review Date:
//    2009-01-09
// 
//  Detailed Comments:
//    -
// 
///////////////////////////////////////////////////////////
module EPTF_Applib_HTTP_Test_Definitions {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_Base_Definitions all;
import from EPTF_CLL_Variable_Definitions all;

import from HTTPmsg_Types all;
import from IPL4asp_Types all;

import from EPTF_Applib_HTTP_Test_Responder all;
import from EPTF_Applib_HTTP_Test_Application1_Definitions all;

//=========================================================================
// Types
//=========================================================================

type component EPTF_HTTP_Test_CT extends EPTF_Base_CT, EPTF_Var_CT
{
  var EPTF_HTTP_Test_Responder_CT v_HTTP_Responder_comp;
  var EPTF_HTTP_Test_Application1_MapperCT v_HTTP_Mapper1_comp;
  var EPTF_HTTP_Test_Application1_LocalCTList v_HTTP_Local1_comps;
  var EPTF_HTTP_Test_Application1_RemoteCTList v_HTTP_Remote1_comps;
  var EPTF_HTTP_Test_Application1_MapperCT v_HTTP_Mapper2_comp;

  var boolean v_isRemote_appl1, v_isRemote_appl2;
  var integer v_numberOfLGens1, v_numberOfLGens2;

  var integer v_nofAnswers := -1;

  var octetstring v_HTTP_oct;
  var HTTPMessage v_HTTP_msg;

  var float v_randState;

  port EPTF_HTTP_Test_CM_PT v_CM_PCO;
}

type component EPTF_HTTP_Test_LocalAPI_CT 
  extends EPTF_HTTP_Test_Application1_LocalCT, EPTF_HTTP_Test_CT 
{
  var ASP_RecvFrom v_ASP_RecvFrom;
  var ASP_Event v_ASP_Event;
  var integer v_noIntWarning;
}

type component EPTF_HTTP_Test_RemoteAPI_CT 
  extends EPTF_HTTP_Test_Application1_RemoteCT, EPTF_HTTP_Test_CT 
{
  var ASP_RecvFrom v_ASP_RecvFrom;
  var ASP_Event v_ASP_Event;
  var integer v_noIntWarning;
  var boolean v_noBoolWarning;
  
  port EPTF_HTTP_Test_Validation_PT v_validation_PCO;
}
}

///////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000-2023 Ericsson Telecom AB
//
// All rights reserved. This program and the accompanying materials
// are made available under the terms of the Eclipse Public License v2.0
// which accompanies this distribution, and is available at
// https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
///////////////////////////////////////////////////////////////////////////////
//
//  File:     EPTF_HTTP_Logger_Definitions.ttcn
//  Rev:      <RnXnn>
//  Prodnr:   CNL 113 618
//  Updated:  2012-11-28
//  Contact:  http://ttcn.ericsson.se
///////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////
// Module: EPTF_HTTP_Logger_Definitions
//
//  Purpose:
//   This module contains the definitions used by the EPTF_HTTP_Logger_CT
//   component.
//
//  Module Parameters:
//   -
//
//  Module depends on:
//    <EPTF_CLL_Base_Definitions>
//
//   <HTTPmsg_Types>
//
//  Current Owner:    
//    EAKOPER 
// 
// Last Review Date:
//   2008-01-09 
///////////////////////////////////////////////////////////
module EPTF_HTTP_Logger_Definitions {

//=========================================================================
// Import Part
//=========================================================================
import from EPTF_CLL_Base_Definitions all;
import from HTTPmsg_Types all;

///////////////////////////////////////////////////////////
// Type: EPTF_HTTP_LoggerClient_FT
//
// Purpose:
//   Function type to register LoggerClient send function 
//
// Elements:
//   pl_message - *in* - *octetstring* - the outgoing octetstring message
//
///////////////////////////////////////////////////////////  
type function EPTF_HTTP_LoggerClient_FT (
  in charstring pl_message) 
runs on self;

///////////////////////////////////////////////////////////
// Type: EPTF_HTTP_LoggerClient_CT 
//
// Purpose:
//   The type definition of the LoggerClient component
//
// Extends:
//   EPTF_Base_CT
//
// Elements:
//   HTTP_LoggerClient_PCO - <EPTF_HTTP_Logger_PT> - Communiucation port between the LoggerClient and The Logger components
//   vf_EPTF_HTTP_LoggerClient_sendFunction - <EPTF_HTTP_LoggerClient_FT> - Function to reference the logging send function
//   vc_EPTF_HTTP_LoggerClient_loggerComponent - <EPTF_HTTP_Logger_CT> - The Logger component
//   v_EPTF_HTTP_LoggerClient_initialized - *boolean* - The component initialized variable
/////////////////////////////////////////////////////////// 
type component EPTF_HTTP_LoggerClient_CT  extends 
  EPTF_Base_CT 
{
  port EPTF_HTTP_Logger_PT        HTTP_LoggerClient_PCO;
  var EPTF_HTTP_LoggerClient_FT vf_EPTF_HTTP_LoggerClient_sendFunction := null;
  var EPTF_HTTP_Logger_CT         vc_EPTF_HTTP_LoggerClient_loggerComponent;
  var boolean                     v_EPTF_HTTP_LoggerClient_initialized := false; 
}

///////////////////////////////////////////////////////////
// Type: EPTF_HTTP_Logger_PT
//
// Purpose:
//   HTTP Logger port type
//
// Detailed Comments:
//   The port receives a HTTP message as octetstring
//
///////////////////////////////////////////////////////////
type port EPTF_HTTP_Logger_PT message {
  inout octetstring
} with { extension "internal" };

///////////////////////////////////////////////////////////
// Type: EPTF_HTTP_Logger_CT
//
// Purpose:
//   HTTP Logger component type
//
// Detailed Comments:
//
///////////////////////////////////////////////////////////
type component EPTF_HTTP_Logger_CT {
  port EPTF_HTTP_Logger_PT HTTP_Logger_PCO;
  var octetstring v_EPTF_HTTP_Logger_incomingMsg;
  var HTTPMessage v_EPTF_HTTP_Logger_decodedMessage;
}

}//end of module
with {
extension "version <RnXnn>"
}

